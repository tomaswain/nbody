/* The Computer Language Benchmarks Game
   https://salsa.debian.org/benchmarksgame-team/benchmarksgame/

   contributed by Mark C. Lewis
   modified slightly by Chad Whipkey
   converted from java to c++,added sse support, by Branimir Maksimovic
   modified by Vaclav Zeman
   modified by Vaclav Haisman to use explicit SSE2 intrinsics
   modified by Tomas Wain to use body scaling and sqrt instead of rsqrt
   modified by Tomas Wain to use AVX instead of SSE
*/
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <array>
#include <immintrin.h>

static const double PI = 3.141592653589793;
static const double SOLAR_MASS = 4 * PI * PI;
static const double DAYS_PER_YEAR = 365.24;
static const double DT = 0.01;
static const double RECIP_DT = 1.0 / DT;
static const __m256d ONE = _mm256_set1_pd(1.0);


class Body {

public:
    double x, y, z, filler, vx, vy, vz, mass;

    Body(){}

    static Body& jupiter(){
        static Body p;
        p.x = 4.84143144246472090e+00;
        p.y = -1.16032004402742839e+00;
        p.z = -1.03622044471123109e-01;
        p.vx = 1.66007664274403694e-03 * DAYS_PER_YEAR;
        p.vy = 7.69901118419740425e-03 * DAYS_PER_YEAR;
        p.vz = -6.90460016972063023e-05 * DAYS_PER_YEAR;
        p.mass = 9.54791938424326609e-04 * SOLAR_MASS;
        return p;
    }

    static Body& saturn(){
        static Body p;
        p.x = 8.34336671824457987e+00;
        p.y = 4.12479856412430479e+00;
        p.z = -4.03523417114321381e-01;
        p.vx = -2.76742510726862411e-03 * DAYS_PER_YEAR;
        p.vy = 4.99852801234917238e-03 * DAYS_PER_YEAR;
        p.vz = 2.30417297573763929e-05 * DAYS_PER_YEAR;
        p.mass = 2.85885980666130812e-04 * SOLAR_MASS;
        return p;
    }

    static Body& uranus(){
        static Body p;
        p.x = 1.28943695621391310e+01;
        p.y = -1.51111514016986312e+01;
        p.z = -2.23307578892655734e-01;
        p.vx = 2.96460137564761618e-03 * DAYS_PER_YEAR;
        p.vy = 2.37847173959480950e-03 * DAYS_PER_YEAR;
        p.vz = -2.96589568540237556e-05 * DAYS_PER_YEAR;
        p.mass = 4.36624404335156298e-05 * SOLAR_MASS;
        return p;
    }

    static Body& neptune(){
        static Body p;
        p.x = 1.53796971148509165e+01;
        p.y = -2.59193146099879641e+01;
        p.z = 1.79258772950371181e-01;
        p.vx = 2.68067772490389322e-03 * DAYS_PER_YEAR;
        p.vy = 1.62824170038242295e-03 * DAYS_PER_YEAR;
        p.vz = -9.51592254519715870e-05 * DAYS_PER_YEAR;
        p.mass = 5.15138902046611451e-05 * SOLAR_MASS;
        return p;
    }

    static Body& sun(){
        static Body p;
        p.mass = SOLAR_MASS;
        return p;
    }

    Body& offsetMomentum(double px, double py, double pz){
        vx = -px / SOLAR_MASS;
        vy = -py / SOLAR_MASS;
        vz = -pz / SOLAR_MASS;
        return *this;
    }
};


class NBodySystem {
private:
    std::array<Body, 5> bodies;

public:
    NBodySystem()
        :  bodies {{
            Body::sun(),
            Body::jupiter(),
            Body::saturn(),
            Body::uranus(),
            Body::neptune()
            }}
	{
        double px = 0.0;
        double py = 0.0;
        double pz = 0.0;
        for(unsigned i=0; i < bodies.size(); ++i) {
            px += bodies[i].vx * bodies[i].mass;
            py += bodies[i].vy * bodies[i].mass;
            pz += bodies[i].vz * bodies[i].mass;
        }
        bodies[0].offsetMomentum(px,py,pz);
    }

    void advance() {
        const unsigned N = (bodies.size()-1)*bodies.size()/2;
        struct __attribute__((aligned(32))) R {
            double dx,dy,dz,filler;
        };
        static R r[1000];
        static __attribute__((aligned(32))) double mag[1000];

        for(unsigned i=0,k=0; i < bodies.size()-1; ++i) {
            Body const & iBody = bodies[i];
            for(unsigned j=i+1; j < bodies.size(); ++j,++k) {
                r[k].dx = iBody.x - bodies[j].x;
                r[k].dy = iBody.y - bodies[j].y;
                r[k].dz = iBody.z - bodies[j].z;
            }
        }

        for(unsigned i=0; i < N; i+=4) {
            __m256d dx,dy,dz;
            dx = _mm256_set_pd(r[i+3].dx, r[i+2].dx, r[i+1].dx, r[i+0].dx);
            dy = _mm256_set_pd(r[i+3].dy, r[i+2].dy, r[i+1].dy, r[i+0].dy);
            dz = _mm256_set_pd(r[i+3].dz, r[i+2].dz, r[i+1].dz, r[i+0].dz);

            //__m256d dSquared = dx*dx + dy*dy + dz*dz;
            __m256d dSquared = _mm256_add_pd(
                _mm256_add_pd(_mm256_mul_pd(dx, dx), _mm256_mul_pd(dy, dy)),
                _mm256_mul_pd(dz, dz));

            __m256d distance = _mm256_sqrt_pd(dSquared);

			__m256d dmag = _mm256_div_pd(ONE,(_mm256_mul_pd(dSquared,distance)));

            _mm256_store_pd(&mag[i],dmag);
        }

        for(unsigned i=0,k=0; i < bodies.size()-1; ++i) {
            Body& iBody = bodies[i];
            for(unsigned j=i+1; j < bodies.size(); ++j,++k) {
                iBody.vx -= r[k].dx * bodies[j].mass * mag[k];
                iBody.vy -= r[k].dy * bodies[j].mass * mag[k];
                iBody.vz -= r[k].dz * bodies[j].mass * mag[k];

                bodies[j].vx += r[k].dx * iBody.mass * mag[k];
                bodies[j].vy += r[k].dy * iBody.mass * mag[k];
                bodies[j].vz += r[k].dz * iBody.mass * mag[k];
            }
        }

        for (unsigned i = 0; i < bodies.size(); ++i) {
            bodies[i].x += bodies[i].vx;
            bodies[i].y += bodies[i].vy;
            bodies[i].z += bodies[i].vz;
        }
    }

    double energy(){
        double e = 0.0;

        for (unsigned i=0; i < bodies.size(); ++i) {
            Body const & iBody = bodies[i];
            double dx, dy, dz, distance;
            e += 0.5 * iBody.mass *
                ( iBody.vx * iBody.vx
                    + iBody.vy * iBody.vy
                    + iBody.vz * iBody.vz );

            for (unsigned j=i+1; j < bodies.size(); ++j) {
                Body const & jBody = bodies[j];
                dx = iBody.x - jBody.x;
                dy = iBody.y - jBody.y;
                dz = iBody.z - jBody.z;

                distance = sqrt(dx*dx + dy*dy + dz*dz);
                e -= (iBody.mass * jBody.mass) / distance;
            }
        }
        return e;
    }

  void scale(double scale) {
    int i;
    
    for (i = 0; i < bodies.size(); i++) {
      Body& iBody = bodies[i];
      iBody.mass *= scale * scale;
      iBody.vx *= scale;
      iBody.vy *= scale;
      iBody.vz *= scale;
    }
  }
};

int main(int argc, char** argv) {
    int n = atoi(argv[1]);

    NBodySystem bodies;
    printf("%.9f\n", bodies.energy());
    bodies.scale(DT);
    for (int i=0; i<n; ++i)
        bodies.advance();
    bodies.scale(RECIP_DT);
    printf("%.9f\n", bodies.energy());
}
